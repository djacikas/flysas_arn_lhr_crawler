﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlySAS_ARN_LHR_Crawler
{
   class Program
   {
      static void Main(string[] args)
      {
         Program p = new Program();
         GetPage getPage = new GetPage();

         var url = "https://classic.flysas.com/";

         var task1 = Task.Run(() => getPage.GetPageAsync(url));
         task1.Wait();
      }
   }
}
