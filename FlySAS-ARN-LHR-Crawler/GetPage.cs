﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FlySAS_ARN_LHR_Crawler
{
   public class GetPage
   {
      public async Task GetPageAsync(string url)
      {
         var baseAddress = new Uri(url);
         var cookieContainer = new CookieContainer();

         using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer, })
         using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
         {
            var responseOne = await client.GetAsync(baseAddress);
            var httpContentOne = await client.GetStringAsync(baseAddress);
            var headersOne = responseOne.Headers.Concat(responseOne.Content.Headers);

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(httpContentOne);

            #region Pasiimu reikiama informacija ir susidedu siuntimui i content kintamaji
            var searchEventTarget = "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$Searchbtn$ButtonLink";
            var eventTargets = htmlDocument.DocumentNode.SelectNodes("//input[contains((@name), 'ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$')]").Select(node => node.Attributes["name"].Value).ToList();
            var destinations = htmlDocument.DocumentNode.Descendants("span").Where(node => node.GetAttributeValue("id", "").Equals("destination-background")).FirstOrDefault().Attributes["data-destlist"].Value.Split(new char[] { ',' }).ToList();
            var hiddenFromDest = "ARN";
            var hiddenToDest = "LHR";
            var __PREVIOUSPAGE = htmlDocument.DocumentNode.SelectSingleNode("//input[@id='__PREVIOUSPAGE']").Attributes["value"].Value;
            var __VIEWSTATE = htmlDocument.DocumentNode.SelectSingleNode("//input[@id='__VIEWSTATE']").Attributes["value"].Value;
            var __VIEWSTATEGENERATOR = htmlDocument.DocumentNode.SelectSingleNode("//input[@id='__VIEWSTATEGENERATOR']").Attributes["value"].Value;

            var parametersPOST1 = new Dictionary<string, string>(){
               { "__EVENTTARGET","ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$Searchbtn$ButtonLink"},
               { "__EVENTARGUMENT",""},

               { "ctl00$FullRegion$TopRegion$_siteHeader$hdnProfilingConsent",""},
               { "ctl00$FullRegion$TopRegion$_siteHeader$hdnTermsConsent",""},
               { "ctl00$FullRegion$TopRegion$_siteHeader$_ssoLogin$MainFormBorderPanel$uid",""},
               { "ctl00$FullRegion$TopRegion$_siteHeader$_ssoLogin$MainFormBorderPanel$pwd",""},
               { "ctl00$FullRegion$TopRegion$_siteHeader$_ssoLogin$MainFormBorderPanel$hdnShowModal",""},
               { "ctl00$FullRegion$TopRegion$_siteHeader$_ssoLogin$MainFormBorderPanel$hdnIsEb0",""},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$ceptravelTypeSelector$TripTypeSelector","roundtrip"},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenIntercont","False"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenDomestic","SE,GB"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenFareType","A"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$txtFrom","Stockholm, Sweden - Arlanda (ARN)"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenFrom","ARN"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$txtTo","London, United Kingdom - Heathrow (LHR)"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenTo","LHR"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$txtFromTOJ",""},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$predictiveSearch$hiddenFromTOJ",""},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenOutbound","2018-09-03"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenReturn","2018-09-09"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hdnSelectedOutboundMonth",""},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hdnSelectedReturnMonth",""},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenReturnCalVisible",""},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$hiddenStoreCalDates","Tue Aug 21 2018 00:00:00 GMT+0300 (FLE Daylight Time),Tue Aug 21 2018 00:00:00 GMT+0300 (FLE Daylight Time),Thu Aug 15 2019 00:00:00 GMT+0300 (FLE Daylight Time)"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$selectOutbound","2018-08-01"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepCalendar$selectReturn","2018-08-01"},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$FlexDateSelector","Show selected dates"},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepPassengerTypes$passengerTypeAdult","1"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepPassengerTypes$passengerTypeChild211","0"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepPassengerTypes$passengerTypeInfant","0"},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$cepNdpFareTypeSelector$ddlFareTypeSelector","A"},

               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$hdnsetDefaultValue","true"},
               { "ctl00$FullRegion$MainRegion$ContentRegion$ContentFullRegion$ContentLeftRegion$CEPGroup1$CEPActive$cepNDPRevBookingArea$hdncalendarDropdown","true"},

               { "__PREVIOUSPAGE", __PREVIOUSPAGE},
               { "__VIEWSTATE",__VIEWSTATE},
               { "__VIEWSTATEGENERATOR",__VIEWSTATEGENERATOR},
            };

            var contentPOST1 = new FormUrlEncodedContent(parametersPOST1);



            #endregion


            cookieContainer.Add(baseAddress, new Cookie("UserLanguage", "en"));
            cookieContainer.Add(baseAddress, new Cookie("UserMarket", "42"));


            //HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
            //httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html, application/xhtml+xml, image/jxr, */*"));
            //request.Content.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data, boundary=---------------------------7e23b91320468");
            //request.Content.("ContentType", "multipart/form-data; boundary=---------------------------7e23b91320468");
            //content.Headers.ContentDisposition = new ContentDispositionHeaderValue("multipart/form-data") { FileName = fileName };
            //content.Headers.ContentDisposition = new ContentDispositionHeaderValue();
            //request.Content = content; //new StringContent(content.ToString(), "multipart/form-data");//CONTENT-TYPE header // isimta Encoding.UTF8,
            //client.DefaultRequestHeaders.Add("Content-Type", "multipart/form-data; boundary=---------------------------7e23b91320468");
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html, application/xhtml+xml, image/jxr, */*"));


            //client.DefaultRequestHeaders.Remove("Expect");
            //contentPOST1.Headers.ContentDisposition = new ContentDispositionHeaderValue("multipart/form-data");
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html, application/xhtml+xml, image/jxr, */*"));

            var responsePOST1T = client.PostAsync(baseAddress, contentPOST1).Result;
            var resultPOST1T = responsePOST1T.Content.ReadAsStringAsync().Result;
            responsePOST1T.EnsureSuccessStatusCode();
            var resultHtmlPOST1T = new HtmlDocument();
            resultHtmlPOST1T.LoadHtml(resultPOST1T);

            /////////////////////////////////////

            //// Task stabdo veikima, del null reference. Manau kad del to jog nepaduodu duomenu i content-disposition
            //HttpRequestMessage requestPOST1 = new HttpRequestMessage(HttpMethod.Post, baseAddress);
            ////requestPOST1.Content.Headers. = new MediaTypeHeaderValue("multipart/form-data");
            //requestPOST1.Content = new FormUrlEncodedContent(parametersPOST1);
            //requestPOST1.Content.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
            //requestPOST1.Headers.Add("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
            //requestPOST1.Headers.Add("Referer", "https://classic.flysas.com/");
            //requestPOST1.Headers.Add("Accept-Language", "lt-LT");
            //requestPOST1.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
            //requestPOST1.Headers.Add("Connection", "Keep-Alive");
            //requestPOST1.Headers.Add("Cache-Control", "no-cache");
            //requestPOST1.Headers.Add("Accept-Encoding", "gzip, deflate");

            //var responsePOST1 = client.SendAsync(requestPOST1).Result; // veikianti
            //var result = responsePOST1.Content.ReadAsStringAsync().Result;
            //responsePOST1.EnsureSuccessStatusCode();
            //var resultHtml = new HtmlDocument();
            //resultHtml.LoadHtml(result);

            /////////////////////////////////////



            #region Ištraukiami parametrai antram POST metodui
            var __EVENTTARGET2 = "btnSubmitAmadeus";
            var __EVENTARGUMENT2 = "";

            var parametersPOST2 = new Dictionary<string, string>();
            parametersPOST2.Add("__EVENTTARGET2", __EVENTTARGET2);
            parametersPOST2.Add("__EVENTARGUMENT2", __EVENTARGUMENT2);

            var parametersNamesSize = resultHtmlPOST1T.DocumentNode.SelectNodes("//input[contains((@type), 'hidden')]").Count;
            var parametersNames = resultHtmlPOST1T.DocumentNode.SelectNodes("//input[contains((@type), 'hidden')]").Select(node => node.Attributes["name"].Value).ToList();
            var parametersValues = resultHtmlPOST1T.DocumentNode.SelectNodes("//input[contains((@type), 'hidden')]").Select(node => node.Attributes["value"].Value).ToList();

            for (int i = 0; i < parametersNamesSize; i++)
            {
               parametersPOST2.Add(parametersNames[i], parametersValues[i]);
            }

            var contentPOST2 = new FormUrlEncodedContent(parametersPOST2);


            #endregion

            Uri uriPOST2 = new Uri("https://book.flysas.com/pl/SASC/wds/Override.action?SO_SITE_EXT_PSPURL=https://classic.sas.dk/SASCredits/SASCreditsPaymentMaster.aspx&SO_SITE_TP_TPC_POST_EOT_WT=50000&SO_SITE_USE_ACK_URL_SERVICE=TRUE&WDS_URL_JSON_POINTS=ebwsprod.flysas.com%2FEAJI%2FEAJIService.aspx&SO_SITE_EBMS_API_SERVERURL=%20https%3A%2F%2F1aebwsprod.flysas.com%2FEBMSPointsInternal%2FEBMSPoints.asmx&WDS_SERVICING_FLOW_TE_SEATMAP=TRUE&WDS_SERVICING_FLOW_TE_XBAG=TRUE&WDS_SERVICING_FLOW_TE_MEAL=TRUE");

            HttpRequestMessage requestPOST2 = new HttpRequestMessage(HttpMethod.Post, uriPOST2);
            //requestPOST1.Content.Headers. = new MediaTypeHeaderValue("multipart/form-data");
            requestPOST2.Content = new FormUrlEncodedContent(parametersPOST2);
            requestPOST2.Headers.Add("Accept", "text/html, application/xhtml+xml, image/jxr, */*");
            requestPOST2.Headers.Add("Referer", "https://classic.flysas.com/en/");
            requestPOST2.Headers.Add("Accept-Language", "lt-LT");
            requestPOST2.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
            requestPOST2.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            requestPOST2.Headers.Add("Host", "book.flysas.com");
            requestPOST2.Headers.Add("Connection", "Keep-Alive");
            requestPOST2.Headers.Add("Cache-Control", "no-cache");
            requestPOST2.Headers.Add("Accept-Encoding", "gzip, deflate");

            var responsePOST2 = client.SendAsync(requestPOST2).Result; // veikianti
            var resultPOST2 = responsePOST2.Content.ReadAsStringAsync().Result;
            var headersPOST2 = responsePOST2.Headers.Concat(responsePOST2.Content.Headers);

            responsePOST2.EnsureSuccessStatusCode();
            var resultHtmlPOST2 = new HtmlDocument();
            resultHtmlPOST2.LoadHtml(resultPOST2);

         }

      }
   }
}
